---
title: "FOSDEM 2016 homenageia Ian Murdock com palestra e vídeo"
kind: article
created_at: 2016-03-12 15:45
author: Paulo Henrique de Lima Santana
---

Durante o [FOSDEM 2016](https://fosdem.org/2016/) realizado nos dias 30 e 31
de janeiro de 2016 na cidade de Bruxelas - Bélgica, o ex-Líder do Projeto
Debian
[Martin Michlmayr](https://fosdem.org/2016/schedule/speaker/martin_michlmayr/)
fez uma palestra em homenagem ao Ian Murdock.

Os detalhes da palestra podem ser visto em:

[https://fosdem.org/2016/schedule/event/ian_murdock](https://fosdem.org/2016/schedule/event/ian_murdock/)

No link acima você encontrará o vídeo da apresentação:

<http://ftp.heanet.ie/mirrors/fosdem-video/2016/janson/ian-murdock.mp4>

E os slides usados:

<https://salsa.debian.org/publicity-team/ian-media/-/blob/master/fosdem-slides/ian-murdock.pdf>

Ao final da apresentação do Martin, é exibido um vídeo com várias imagens do
Ian. Este vídeo foi elaborado pelo nosso colega brasileiro
[Valéssio Brito](http://valessiobrito.com.br/)
com a ajuda de várias pessoas da comunidade que enviaram fotos para a sua
montagem. Há inclusive fotos do Ian durante o
[FISL - Fórum Internacional Software Livre](http://www.fisl.org.br)
em 2004.

Este vídeo pode ser visto em:

<https://peertube.debian.social/videos/watch/12bf0541-36dd-4f2d-8ee1-09b1ddbae112>

Créditos e fonte do vídeo:

<http://deb.li/ian>