---
title: "Seminário on-line para a UFAL"
kind: article
created_at: 2021-06-27 15:00
author: Thiago Pezzo e Paulo Henrique de Lima Santana
---

No feriado de 3 de junho de 2021, o DD Paulo Santana (phls) e o contribuidor
Thiago Pezzo (tico) realizaram um seminário on-line para o curso de
[Serviço Social](https://fsso.ufal.br/extensao/programa-de-extensao) da
[Universidade Federal de Alagoas (UFAL)](https://ufal.br/).

A apresentação teve como objetivo difundir o sistema operacional e a comunidade
[Debian](https://www.debian.org), como também fazer uma introdução aos
sistemas operacionais, movimento Software Livre e licenças de software para um
público não especializado. Foram abordadas situações nas quais o Debian
GNU/Linux poderia ser utilizado na prática profissional de assistentes sociais.

Também participaram do seminário professores(as) e alunos(as) de escolas
públicas parceiras em projetos de extensão. Agradecemos à Prof. Dra. Telma
Sasso pela oportunidade! Esperamos que estudantes e profissionais do Serviço
Social fiquem interessados(as) em conhecer e futuramente em contribuir para
nossa [comunidade Debian Brasil](https://debianbrasil.org.br).

Você pode assistir a gravação [aqui](https://www.youtube.com/watch?v=PwPZ3rImW84)

![Seminário UFAL](/blog/imagens/seminario-online-ufal.jpg =400x)
