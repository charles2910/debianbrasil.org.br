---
title: "Chamada de trabalhos para o Open Day na DebConf19"
kind: article
created_at: 2019-03-25 14:30
author: Paulo Henrique de Lima Santana
---

A [DebConf19 - Conferênca Mundial de Desenvolvedores(as) do Projeto Debian](https://debconf19.debconf.org/),
acontecerá de 21 a 28 de julho de 2019 no Campus central da Universidade
Tecnológica Federal do Paraná - UTFPR, em Curitiba - PR.

No dia 20 de julho (sábado) acontecerá o tradicional **Open Day (Dia Livre)**,
que é um dia dedicado a palestras em português sobre:

- Software Livre e Código Aberto.
- Linguagens, ferramentas, CMS, etc.
- Introdução ao Debian.

Se você tem interesse em contribuir para a realização do Open Day, envie a sua
proposta de atividade até o dia 28 de abril no link abaixo:

<https://debconf19.debconf.org/talks/new>

- No campo "Track" escolha a opção: Introduction to Free Software & Debian.
- O campo Abstract pode ser preenchido em português.

Para mais informações sobre o Open Day, acesse:

<https://debconf19.debconf.org/pt-br/openday>

Se você quiser palestrar em inglês sobre temas relacionados ao Debian, fique
a vontade para enviar propostas de atividades para a programação normal de
atividades da DebConf19 que acontecerá de 21 a 27 de julho. Mais informações:

<https://debconf19.debconf.org/cfp>