---
title: "The Federal Police of Brazil thank the Debian project"
kind: article
created_at: 2016-11-29 12:20
author: Paulo Henrique de Lima Santana
---

On the
[Federal Police of Brazil](https://en.wikipedia.org/wiki/Federal_Police_of_Brazil)
website there is a page where they thank several free software communities,
including Debian. We reproduce the text below translated by
[Valéssio Brito](http://valessiobrito.com.br/).

**Free Software at the Federal Police**

The Web Portals of the [Federal Police](http://www.pf.gov.br) are developed,
predominantly, with free software.

The Federal Police thank the communities (people, companies, public bodies,
NGOs, universities) that created and maintain the following free and
predominantly free software used in the creation of this portal:

 [Debian](https://www.debian.org/) - Distribution of the GNU Operating System,
  initiated by Ian Murdock, in
[1993](https://www.debian.org/doc/manuals/project-history/ch4.pt.html).

Source: <http://www.pf.gov.br/software-livre>

![Simpbolo pf](/blog/imagens/simpbolo-pf.png)