---
title: "Retrospectiva FISL15: Encontro Comunitário Debian Brasil"
kind: article
created_at: 2014-06-19 14:49
author: Paulo Santanta
---

![](/blog/imagens/logo-debian.png)

**Resumo:**

Proposta para encontro comunitário Debian Brasil no FISL 15.

**Proposta:**

Programação do encontro comunitário:

-   Apresentação dos objetivos dos Grupos de Usuários Debian (GUD).
-   Articulação de grupos Estaduais de usuários Debian.
-   Planejamento sobre o site de comunidade e do wiki
-   Planos de contingência para os sites comunidade e wiki
-   Planejamento da linha de atuação até o próximo FISL 15.

Links relevantes do grupo a partir de <http://debianbrasil.org.br>

**Autor:**

-   André Felipe Machado

**Gravação:**

- [YouTube](https://www.youtube.com/watch?v=bSTTqjqOBr4)
- [Peertube](https://peertube.debian.social/videos/watch/f032d380-d235-449b-8c43-786a2f6acf0f)

Observação: o resumo, a proposta e os autores foram retirados do texto
original enviado no
[papers](http://papers.softwarelivre.org/papers_ng/public/new_grid?day=7).
A atividade pode ter sofrido alterações durante a sua realização.
