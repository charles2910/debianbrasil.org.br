---
title: "Chamada de atividades para DebConf21 - incluindo trilha em português"
kind: article
created_at: 2021-06-10 14:00
author: Paulo Henrique de Lima Santana
---

![](/blog/imagens/dc21-logo.png =200x)

A equipe de conteúdos da DebConf convida todas as pessoas interessas para
enviarem propostas de atividades para a
[DebConf21 - Conferência mundial de desenvolvedores(as) do Projeto Debian](https://debconf21.debconf.org/),
que acontecerá online de 22 a 29 de agosto de 2021.

**Este ano teremos uma trilha de atividades em português inseridas dentro da
programação oficial da DebConf21.**

## Como enviar uma atividade

Agora você já pode enviar uma [proposta de atividade](https://debconf21.debconf.org/talks/new/)
em inglês (para a programação geral) ou em português (para a trilha específica).
As atividades não se limitam a palestras tradicionais ou sessões informais
(BoFs): aceitamos submissões de tutoriais, performances, instalações artísticas,
debates ou qualquer outro formato de atividade que você acha que será de
interesse da comunidade Debian.

As atividades regulares podem ter 20 ou 45 minutos de duração (incluindo tempo
para perguntas), outros tipos de atividades (workshops, demonstrações, lightning
talks, etc) podem ter durações diferentes. Por favor escolha a duração mais
adequada para a sua atividade e explique qualquer pedido especial.

Para enviar uma atividade, você precisará criar uma conta no site da DebConf21.
Sugerimos que as pessoas que possuem contas no Debian Salsa (incluindo DDs e DMs)
usem seu [login](https://salsa.debian.org) para criar uma conta. No entanto,
isso não é obrigatório, pois você pode se inscrever usando um endereço de
e-mail e uma senha.

Para enviar uma proposta de atividade para a trilha em português, no formulário
de envio de proposta selecione "Português" no campo "Language". Nesse caso os
outros campos como "Title" (título) e "Abstract" (descrição) devem ser
preenchidos em português. Assim saberemos que a sua atividade será realizada em
português e agendaremos na trilha.

## Prazos

* Prazo final para de envio das propostas: 20 de junho (domingo)
* Notificação de aceite:  4 de julho (domingo)
* Prazo flexível para envio das pré-gravações: 8 de agosto (domingo)
* Prazo final para envio das pré-gravações: 18 de agosto (quarta-feira)

Será possível enviar propostas após o prazo, mas elas serão consideradas caso a
caso.

As gravações enviadas dentro do prazo flexível tem a garantia de serem revisadas
com tempo suficiente para serem corrigidas, se necessário. Vídeos enviados
depois disso terão menos atenção quanto mais perto estivermos da Conferência.
Não serão aceitas gravações após o prazo final de 18 de agosto; você terá que
apresentar ao vivo (e enfrentar as consequências de fazer isso).

## Tópicos e trilhas

Embora convidamos para o envio de propostas sobre qualquer assunto relacionado
ao Debian ou FLOSS, temos alguns tópicos gerais sobre os quais encorajamos as
pessoas a enviar propostas, incluindo, mas não se limitando a:

- Introdução ao Software Livre e Debian;
- Empacotameno, política (policy) e infraestrutura Debian;
- Administração de sistemas, automação e orquestração;
- Cloud e conteineres;
- Segurança;
- Comunidade, diversidade, inclusão local e contexto social;
- Internacionalização, localização e acessibilidade;
- Embarcados & Kernel;
- Debian em artes e ciências;
- Debian Blends e distribuições derivadas do Debian;

## Fusos horários

Como uma Conferência online global, a programação deve se ajustar aos fusos
horários dos participantes. Indique o fuso horário do qual você participará,
bem como sua disponibilidade. Se você trabalhará junto com pessoas de outros
fusos horários (por exemplo, em uma sessão BoF), indique uma janela de horário
preferencial para agendamento (usando horários UTC).

A trilha em português acontecerá das 19h às 22h no horário de Brasília.

## Palestras online

Para palestras regulares com estilo de apresentação, como no ano passado, há 2
possibilidades: você pode pré-gravar sua palestra e ficar disponível para uma
sessão de perguntas e respostas ao vivo após a apresentação, ou fazê-la ao vivo
por videoconferência. A palestra pré-gravada com perguntas e respostas ao vivo
é preferível, pois nos dá menos incertezas com relação à conectividade e garante
uma qualidade geral mais alta. Se você decidir fazer sua palestra ao vivo,
precisamos estar preparados para que ela falhe se algo der errado durante a sua
palestra.

Obviamente, a pré-gravação faz pouco sentido para BoFs e outros tipos de sessões
interativas.

## Ajuda para envio de propostas no IRC

Se você deseja enviar uma proposta de atividade, mas gostaria de discuti-la
primeiro, sinta-se à vontade para conversar no canal de IRC #debian-br no OFTC,
onde algumas pessoas estarão disponíveis para ajudar palestrantes em potencial
a preparar suas propostas de atividade para a DebConf21.

Apenas certifique-se de ficar no canal se não obtiver uma resposta imediata,
porque podemos demorar um pouco para responder.

## Código de Conduta

A DebCon21 é coberta por um [Código de Conduta](https://debconf.org/codeofconduct.shtml)
projetado para garantir a segurança e o conforto de todas as pessoas. O código
se aplica a todos(as) os(as) participantes, incluindo palestrantes e o conteúdo
de suas apresentações. Não hesite em nos contatar (mesmo em português) em
<content@debconf.org> se você tiver alguma dúvida ou não tiver certeza sobre
determinado conteúdo que gostaria de apresentar.

## Cobertura de vídeo

Como uma conferência online, as atvidades serão transmitidas ao vivo pela
internet. E a menos que o(a) palestrante opte diferente, todas as atividades
programadas serão gravadas e publicadas posteriormente sob a
[licença da DebConf](https://meetings-archive.debian.net/pub/debian-meetings/LICENSE)
(MIT/Expat), bem como os slides e os artigos da apresentação, sempre que
disponíveis.

Observe que, embora possamos divulgar o seu desejo aos participantes, não
podemos evitar que as pessoas gravem e publiquem sua palestra na transmissão ao
vivo.

## Nota de encerramento

DebConf21 está aceitando patrocinadores; se você estiver interessado ou achar
que conhece outras pessoas/empresas/entidades que estariam dispostos a ajudar,
por favor, entre em contato com <sponsors@debconf.org>

Em caso de dúvidas ou se você quiser trocar algumas ideias conosco primeiro,
não hesite em entrar em contato (mesmo em português) com a equipe de conteúdo
em <content@debconf.org>

Esperamos ver você online em breve!

A equipe DebConf21