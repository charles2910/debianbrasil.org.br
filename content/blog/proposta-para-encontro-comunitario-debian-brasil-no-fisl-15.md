---
title: "Proposta para encontro comunitário Debian Brasil no FISL 15"
kind: article
created_at: 2014-03-09 23:06
author: Andre Felipe Machado
---

A proposta de encontro comunitário do grupo de usuários Debian Brasil durante
o FISL 15 terá as seguintes características

Público estimado: 20 pessoas.

Tempo estimado: 110 minutos.

[Proposta de espaço de usuários](/blog/proposta-para-grupo-de-usuarios-debian-no-fisl-15/)

Programação do encontro comunitário:

-   apresentação dos objetivos dos Grupos de Usuários Debian (GUD).
-   articulação de grupos Estaduais de usuários Debian.
-   planejamento sobre o site de comunidade e do wiki.
-   planos de contingência para os sites comunidade e wiki.
-   planejamento da linha de atuação até o próximo FISL 15.

Links relevantes do grupo a partir de <http://debianbrasil.org.br>

Coordenadores (também listados como comentários neste artigo):

André Felipe Machado, afmachado1963 EM gmail.com

