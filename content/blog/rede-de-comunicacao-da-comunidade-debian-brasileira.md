---
title: "Rede de comunicação da comunidade Debian brasileira"
kind: article
created_at: 2021-04-12 21:00
author: Paulo Henrique de Lima Santana
---

## 1) Canais no telegram

Debian notícias em português:

<https://t.me/DebianBrasilNoticias>

Debian Brasil:

<https://t.me/debianbrasil>

Debian Brasília

<https://t.me/debianbrasilia>

Debian Curitiba

<https://t.me/debiancwb>

Debian Minas Gerais

<https://t.me/debianmg>

Debian Rio de Janeiro

<https://t.me/debianrj>

Debian São Paulo

<https://t.me/DebianSP>

Debian Educacional Brasil

<https://t.me/DebianEduBR>

Debian equipe tradução para o português

<https://t.me/debl10nptBR>

Debian Art & Design

<https://t.me/debiandesign>

## 2) Canais IRC na rede OFTC

Debian Brasil:

\#debian-br

Debian Brasília

\#debian-bsb

Debian Minas Gerais

\#debian-mg

Debian São Paulo

\#debian-sp

Debian equipe de tradução para o português

\#debian-l10n-br

Debian eventos no Brasil

\#debian-br-eventos

Debian Devel em português

\#debian-devel-br

## 3) Listas de discussão

Debian Brasil

<https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-geral>

Debian Ceará

<https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-gud-ce>

Debian Curitiba

<https://lists.alioth.debian.org/mailman/listinfo/debian-br-gud-curitiba>

Debian Rio Grande do Sul

<https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-gud-rs>

Debian Devel em português

<https://lists.debian.org/debian-devel-portuguese>

Debian equipe de tradução para o português

<https://lists.debian.org/debian-l10n-portuguese>

Debian usuários(as) em português

<https://lists.debian.org/debian-user-portuguese>

Debian eventos no Brasil

<https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-eventos>

Debian notícias em português

<https://lists.debian.org/debian-news-portuguese>