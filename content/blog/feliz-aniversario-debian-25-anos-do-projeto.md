---
title: "Feliz aniversário Debian! 25 anos do Projeto"
kind: article
created_at: 2018-08-16 17:05
author: Paulo Henrique de Lima Santana
---

Hoje (16/08/2018) o [Projeto Debian](https://www.debian.org/) completa 25 anos
de existência.

Feliz aniversário Debian!

E obrigado a todos(as) os(as) contribuidores(as) que fazem deste o maior projeto
de Software Livre do mundo.

<https://bits.debian.org/2018/08/debian-is-25-pt-BR.html>

![Debian25years](/blog/imagens/debian25years =400x)


