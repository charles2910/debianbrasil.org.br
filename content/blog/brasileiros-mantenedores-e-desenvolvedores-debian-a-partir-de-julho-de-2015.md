---
title: "Brasileiros(as) Mantenedores(as) e Desenvolvedores(as) Debian a partir de julho de 2015"
kind: article
created_at: 2022-11-09 00:01
author: Paulo Henrique de Lima Santana
---

Desde de setembro de 2015, o
[time de publicidade](https://wiki.debian.org/Teams/Publicity)
do [Projeto Debian](https://www.debian.org) passou a publicar a cada dois meses
[listas](https://bits.debian.org/tag/project.html) com os nomes dos(as) novos(as)
[Desenvolvedores(as) Debian](https://wiki.debian.org/DebianDeveloper) (DD - do
inglês *Debian Developer*) e
[Mantenedores(as) Debian](https://wiki.debian.org/DebianMaintainer)
(DM - do inglês *Debian Maintainer*).

Estamos aproveitando estas listas para publicar abaixo os nomes dos(as)
brasileiros(as) que se tornaram Desenvolvedores(as) e Mantenedores(as) Debian a
partir de julho de 2015.

## Desenvolvedores(as) Debian / Debian Developers / DDs:

**David da Silva Polverari**

- [DDPO](https://qa.debian.org/developer.php?login=david.polverari@gmail.com)
- Desde julho de 2023
- Cidade de residência: Brasília - DF

**Marcos Talau**

- [DDPO](https://qa.debian.org/developer.php?login=talau@debian.org)
- Desde agosto de 2022
- Cidade de residência: Curitiba - PR

**Francisco Vilmar Cardoso Ruviaro**

- [DDPO](https://qa.debian.org/developer.php?login=vilmar@debian.org)
- Desde fevereiro de 2022
- Cidade de residência: Santa Maria - RS

**Fabio Augusto De Muzio Tobich**

- [DDPO](https://qa.debian.org/developer.php?login=ftobich%40debian.org)
- Desde maio de 2021
- Cidade de residência: Curitiba - PR

**Gabriel F. T. Gomes**

- [DDPO](https://qa.debian.org/developer.php?login=gabriel@debian.org)
- Desde maio de 2020
- Cidade de residência: Campinas - SP

**Thiago Andrade Marques**

- [DDPO](https://qa.debian.org/developer.php?login=andrade@debian.org)
- Desde maio de 2020
- Cidade de residência: Campinas - SP

**Márcio de Souza Oliveira**

- [DDPO](https://qa.debian.org/developer.php?login=marciosouza@debian.org)
- Desde fevereiro de 2019
- Cidade de residência: Brasília - DF

**Paulo Henrique de Lima Santana**

- [DDPO](https://qa.debian.org/developer.php?login=phls@debian.org)
- Desde janeiro de 2019
- Cidade de residência: Belo Horizonte - MG

**Samuel Henrique**

- [DDPO](https://qa.debian.org/developer.php?login=samueloph@debian.org)
- Desde abril de 2018
- Cidade de residência: Dublin - Irlanda

**Sérgio Durigan Júnior**

- [DDPO](https://qa.debian.org/developer.php?login=sergiodj@debian.org)
- Desde março de 2018
- Cidade de residência: Toronto - Canadá

**Daniel Lenharo de Souza**

- [DDPO](https://qa.debian.org/developer.php?login=lenharo@debian.org)
- Desde maio de 2017
- Cidade de residência: Curitiba - PR

**Giovani Augusto Ferreira**

- [DDPO](https://qa.debian.org/developer.php?login=giovani@debian.org)
- Desde dezembro de 2016
- Cidade de residência: Poço Fundo - MG

**Adriano Rafael Gomes**

- Desde agosto de 2016
- Cidade de residência: Novo Hamburgo - RS

**Breno Leitão**

- [DDPO](https://qa.debian.org/developer.php?login=brenohl@br.ibm.com)
- Desde agosto de 2016
- Cidade de residência: Campinas - SP

**Lucas Kanashiro**

- [DDPO](https://qa.debian.org/developer.php?login=kanashiro.duarte@gmail.com)
- Desde junho de 2016
- Cidade de residência: Brasília - DF

**Herbert Parentes Fortes Neto**

- [DDPO](https://qa.debian.org/developer.php?login=hpfn@ig.com.br)
- Desde junho de 2016
- Cidade de residência: Rio de Janeiro – RJ

## Mantenedores(as) Debian / Debian Maintainers / DMs:

**Carlos Henrique Lima Melara**

- [DDPO](https://qa.debian.org/developer.php?login=charlesmelara@riseup.net)
- Desde abril de 2023
- Cidade de residência: Campinas - SP

**Josenilson Ferreira da Silva**

- [DDPO](https://qa.debian.org/developer.php?login=nilsonfsilva@hotmail.com)
- Desde janeiro de 2023
- Cidade de residência: Recife - PE

**Braulio Henrique Marques Souto**

- [DDPO](https://qa.debian.org/developer.php?login=braulio@disroot.org)
- Desde novembro de 2022
- Cidade de residência:

**Guilherme de Paula Xavier Segundo**

- [DDPO](https://qa.debian.org/developer.php?login=guilherme.lnx@gmail.com)
- Desde abril de 2022
- Cidade de residência: Campo Mourão - PR

**Paulo Roberto Alves de Oliveira**

- [DDPO](https://qa.debian.org/developer.php?login=kretcheu@gmail.com)
- Desde novembro de 2021
- Cidade de residência: São Vicente - SP

**Sergio Almeida Cipriano Junior**

- [DDPO](https://qa.debian.org/developer.php?login=sergiosacj%40riseup.net)
- Desde novembro de 2021
- Cidade de residência: Brasília - DF

**William Grzybowski**

- [DDPO](https://qa.debian.org/developer.php?login=william@grzy.org)
- Desde dezembro de 2019
- Cidade de residência: Curitiba - PR

**Tiago Ilieve**

- [DDPO](https://qa.debian.org/developer.php?login=tiago.myhro@gmail.com)
- Desde maio de 2016
- Cidade de residência: Belo Horizonte - MG

------------------------------------------------------------------------

Observações:

1. Esta lista será atualizada quando o time de publicidade do Debian publicar
novas listas com DMs e DDs e tiver brasileiros.
2. Para ver a lista completa de Mantenedores(as) e Desenvolvedores(as) Debian,
inclusive outros(as) brasileiros(as) antes de julho de 2015 acesse:
<https://nm.debian.org/public/people>
