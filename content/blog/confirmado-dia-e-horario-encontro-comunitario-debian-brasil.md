---
title: "Confirmado dia e horário Encontro Comunitário Debian Brasil"
kind: article
created_at: 2014-04-03 13:05
author: Andre Felipe Machado
---

A organização do FISL 15 confirmou a realização do encontro comunitário do
Debian Brasil.

atividade: Encontro Comunitário Debian Brasil no FISL 15

- sala: 41E
- dia: 07/05/2014
- horário: 09h00
- duração: 120 minutos

 \* A atividade deve ter a duração máxima de 110 minutos  dentro dos quais
deve ser considerado o espaço para debate e perguntas  do público. Os 10
minutos restantes são reservados para que a troca  de atividades ocorra sem
atrasar o restante da programação.

Proponha mais tópicos para debates aqui nos comentários abaixo , além dos já
listados no
[programa inicial](/blog/proposta-para-encontro-comunitario-debian-brasil-no-fisl-15/)
ou detalhando-os.
