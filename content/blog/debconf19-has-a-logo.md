---
title: "DebConf19 has a logo"
kind: article
created_at: 2018-06-06 15:41
author: Paulo Henrique de Lima Santana
---

The DebConf19 organization team is proud to announce the logo contest winner,
DC19 will take place in Curitiba on July 2019.

We received 11
[logos proposals](https://wiki.debian.org/DebConf/19/Artwork/LogoProposals),
some of them referencing Curitiba and others Brazil. After that, there was a
public poll where anyone could vote in their three favorite logo, then the votes
were counted and the
[final result](https://salsa.debian.org/debconf-team/public/data/dc19/-/blob/master/artwork/debconf19-brand/proposals/resultado-concurso-logo-debconf19.pdf)
gave us the [winner logo](https://wiki.debian.org/DebConf/19/Artwork) it
was made by our fellow Brazilian Ramon Mulin and it combined the word DebConf
with a
["Tamanduá-bandeira" (Banner Ant-eater)](https://en.wikipedia.org/wiki/Giant_anteater).

The "Tamanduá-bandeira" is one of the four biggest species of anteaters finded
on Central America and South America, and is a species in danger of extintion.
It's easily recognized by its long nose and extended tongue, this helps
Tamanduá-bandeira on its diet, that consists of ants and termites, and also the
animal is one of the brazilian symbols.

We thank each person who made their time available to make and submit their
proposal, and the people who gently participated in the voting process. These
initiatives make the DebConf19 organization feel confident that the community
will support and help in holding a great event in Brazil!

![](/blog/imagens/dc19-logo-horizontal.png =400x)