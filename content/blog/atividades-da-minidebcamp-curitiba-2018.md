---
title: "Atividades da MiniDebCamp Curitiba 2018"
kind: article
created_at: 2018-04-06 23:53
author: Giovani Ferreira
---

A terceira edição da [MiniDebConf 2018](https://minidebconf.curitiba.br/2018/)
contará com 2 dias de MiniDebCamp, entre os dias 11 e 12 de abril e contará
exclusivamente com atividades práticas e visa principalmente o contato direto
entre os diversos colaboradores e novos contribuidores.]

Sabemos que muitas vezes as pessoas precisam de um apoio extra para começar a
contribuir dentro de um projeto de software livre e por isso estamos trazendo
diversas atividades para proporcionar uma agradável experiência de colaboração
com Debian.

Durante a MiniDebCamp teremos atividades e Hacking nas seguintes trilhas:

- **Acolhimento de novos colaboradores** - Quer contribuir para o Debian mas
não sabe como? Vem trocar uma ideia com a gente.
- **Organizaçao da DebConf19** - A DebConf19 acontecerá em Curitiba em 2019,
então vamos iniciar nosso planejamento.
- **BSP - Bug Squashing Party** - A nossa
[festa de caça a bugs](https://wiki.debian.org/BSP/BeginnersHOWTO).
- **Migração Salsa** - Com o encerramento do repositório Alioth (FusionForge)
vamos migrar os repositórios de pacotes ófãos para Salsa (GitLab)
- **Criação de chaves PGP.**
- **Tradução** - Ajude a [traduzir o Debian](https://wiki.debian.org/Brasil/Traduzir).
- **Empacotamento** - Assista as
[videoaulas sobre empacotamento de software no Debian](http://eriberto.pro.br/wiki/index.php?title=Algumas_coisas_sobre_Debian...)
produzidos pelo João Eriberto e venha tirar dúvidas com ele.

Lembrando que várias dessas atividades continuarão por todos os dias da
MiniDebConf e portanto este é o momento de começar a contribuir com o
“Sistema Operacional Universal”

Venha para a MiniDebConf Curitiba 2018 que acontecerá de 11 a 14 de abril no
Campus Central da UTFPR - Universidade Tecnológica Federal do Paraná, Av. Sete de Setembro, 3165.


