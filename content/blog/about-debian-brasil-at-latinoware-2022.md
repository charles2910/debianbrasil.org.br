---
title: "About Debian Brasil at Latinoware 2022"
kind: article
created_at: 2022-11-12 19:00
author: Paulo Henrique de Lima Santana
---

From November 2nd to 4th, 2022, the 19th edition of
[Latinoware](https://latinoware.org/) - Latin American Congress of Free Software
and Open Technologies took place in Foz do Iguaçu. After 2 years happening
online due to the COVID-19 pandemic, the event was back in person and we felt
[Debian Brasil](https://debianbrasil.org.br) community should be there.
Out last time at Latinoware was in
[2016](https://debianbrasil.org.br/blog/about-debian-brasil-at-latinoware-2016/)

The Latinoware organization provided the Debian Brazil community with a booth
so that we could have contact with people visiting the open exhibition area and
thus publicize the Debian project. During the 3 days of the event, the booth was
organized by me (Paulo Henrique Santana) as Debian Developer, and by Leonardo
Rodrigues as Debian contributor. Unfortunately Daniel Lenharo had an issue and
could not travel to Foz do Iguaçu (we miss you there!).

![Latinoware 2022 booth 1](https://debianbrasil.org.br/blog/imagens/latinoware-2022-01.jpg =400x)

A huge number of people visited the booth, and the beginners (mainly students)
who didn't know Debian, asked what our group was about and we explained various
concepts such as what Free Software is, GNU/Linux distribution and Debian
itself. We also received people from the Brazilian Free Software community and
from other Latin American countries who were already using a GNU/Linux
distribution and, of course, many people who were already using Debian. We had
some special visitors as Jon maddog Hall, Debian Developer Emeritus Otávio
Salvador, Debian Developer Eriberto Mota, and Debian Maintainers Guilherme de
Paula Segundo and Paulo Kretcheu.

![Latinoware 2022 booth 4](https://debianbrasil.org.br/blog/imagens/latinoware-2022-04.jpg =400x)

Photo from left to right: Leonardo, Paulo, Eriberto and Otávio.

![Latinoware 2022 estande 5](https://debianbrasil.org.br/blog/imagens/latinoware-2022-05.jpg =400x)

Photo from left to right: Paulo, Fabian (Argentina) and Leonardo.

In addition to talking a lot, we distributed Debian stickers that were produced
a few months ago with Debian's sponsorship to be distributed at DebConf22
(and that were left over), and we sold several Debian
[t-shirts]((http://loja.curitibalivre.org.br/)) produced by
[Curitiba Livre community]((http://curitibalivre.org.br)).

![Latinoware 2022 booth 2](https://debianbrasil.org.br/blog/imagens/latinoware-2022-02.jpg =400x)

![Latinoware 2022 booth 3](https://debianbrasil.org.br/blog/imagens/latinoware-2022-03.jpg =400x)

We also had 3 talks included in Latinoware official schedule.
[I]((https://latinoware.org/paulo-henrique-de-lima-santana/)) talked about:
"how to become a Debian contributor by doing translations" and "how the
SysAdmins of a global company use Debian". And
[Leonardo]((https://latinoware.org/leonardo-rodrigues-pereira/)) talked about:
"advantages of Open Source telephony in companies".

![Latinoware 2022 booth 6](https://debianbrasil.org.br/blog/imagens/latinoware-2022-06.jpg =400x)

Photo Paulo in his talk.

Many thanks to Latinoware organization for once again welcoming the Debian
community and kindly providing spaces for our participation, and we
congratulate all the people involved in the organization for the success of
this important event for our community. We hope to be present again in 2023.

We also thank Jonathan Carter for approving financial support from Debian for
our participation at Latinoware.

[Portuguese version](https://debianbrasil.org.br/blog/participacao-da-comunidade-debian-no-latinoware-2022/)
