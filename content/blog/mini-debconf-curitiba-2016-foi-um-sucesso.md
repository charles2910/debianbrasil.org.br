---
title: "MiniDebConf Curitiba 2016 foi um sucesso"
kind: article
created_at: 2016-03-09 02:52
author: Daniel lenahro
---

![MiniDebConf Curitiba 2016](/blog/imagens/2016minidebcwb_media.jpg)

Prezad@s colegas, participantes e envolvido/as,

A  [MiniDebConf Curitiba 2016](http://br2016.mini.debconf.org/) foi um sucesso!
Tivemos ao longo de 2 dias de dedicação ao
[Projeto Debian](https://www.debian.org/) 20 horas de programação. Foram 85
pessoas presentes aproveitando 12 palestras,
07 Lightning Talks e 05 Workshops.

A organização vem, com enorme alegria e satisfação, agradecer a  importante
participação, presença e confiança de cada um@ de vocês.

Agradecemos aos palestrantes por dedicarem seu conhecimento para preparar as
apresentações e compartilhar com o público presente.

Agradecemos a cada um(a) dos(as) participantes que acreditaram no evento e
compareceram e contribuíram para aproximar mais a comunidade Debian.

Agradecemos à [Aldeia Coworking](http://aldeiacoworking.com.br/) por gentilmente
ceder o espaço físico e colaborar para a realização do evento.

Por  fim, mas não menos importante, agradecemos A TODA EQUIPE dedicada que
trabalhou com afinco manhã, tarde, noite e madrugada, meses antes, semanas antes,
dias antes do evento iniciar. Cada membro desta  equipe superou limites, superou
expectativas, buscando o sorriso no rosto com o exercício da solidariedade na
solução de qualquer problema  que se apresentasse. Esperamos que, de alguma
forma, a MiniDebConf Curitiba 2016 possa ter colaborado para o seu crescimento
pessoa e profissional.

A organização convida a todos os envolvidos neste evento a investirem energia
na continuidade do espirito colaborativo e fortalecimento da comunidade Debian
e do Software Livre no Brasil e no mundo.

Obrigado!!