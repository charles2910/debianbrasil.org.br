---
title: "Links da live Debian Brasil de 11/05/2021"
kind: article
created_at: 2021-05-12 9:00
author: Paulo Henrique de Lima Santana
---

A seguir estão os links citados na live do
[canal Debian Brasil no YouTube](https://www.youtube.com/debianbrasiloficial)
do dia 11 de maio de 2021.

[Vídeo](https://www.youtube.com/watch?v=cSMrbOQjjdY) para assistir.

Participantes:

* [Daniel Lenharo](https://www.sombra.eti.br)
* [Fred Guimarães](https://vilarejo.pro.br)
* [Paulo Santana](https://phls.com.br)
* [Ramon Mulin](http://blogdomulin.com.br)

DebianEdu

* Site: <https://wiki.debian.org/DebianEdu/>
* Tradução: <https://hosted.weblate.org/projects/debian-edu-documentation/debian-edu-bullseye/>

Palestra: Skolelinux and Debian Edu Quo Vaids?

* [Peertube](https://peertube.debian.social/videos/watch/3d46c4c1-de09-494e-a83c-ba9bf82bfbde)
* [YouTube](https://www.youtube.com/watch?v=kkcEi4607Zs)

Palestra do Mulin "Software Livre para uma educação libertadora"

* 19/05 às 19h
* <https://www.youtube.com/ieducadigital>

Grupo KDE Brasil no telegram

* <https://t.me/kdebrasil>

EOMA68 Computing Devices

* <https://www.crowdsupply.com/eoma68/micro-desktop>