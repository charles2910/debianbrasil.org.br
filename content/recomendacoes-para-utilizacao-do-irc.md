---
title: "Recomendações para utilização do IRC"
kind: page
created_at: 2020-09-13 20:09
author: Paulo Henrique de Lima Santana
---

# Recomendações para utilização do IRC

Esse documento é fortemente baseado nas
[regras de Conduta da Freenode](http://freenode.net/policy.shtml) e no
[Regras de Conduta para o canal do Debian-BR](http://www.debianbrasil.org/docs/nacionais/sgml/canal/canal.html/index.html)
criado pelo Debian Brasil.

Nosso canal segue as regras da Freenode. Deve-se observar especialmente o
seguinte parágrafo da Policy:

Illegal activities, warez, porn, noticeably heavy mp3 trading, hax0r activity
and various types of antisocial behavior are all off-topic on Open Projects
Net and may result in your being barred from access. Please respect our rules.

Traduzindo:

Atividades ilegais, warez, pornografia, trocas de mp3 em quantidades notáveis,
atividade hax0r e vários tipos de comportamento anti-social, são todos fora de
tópico na Rede Open Projects (!FreeNode) e podem resultar em no bloqueio do seu
acesso. Por favor, respeite nossas regras.

Evite a utilização de "scripts" para canais.

- Evite a utilização de "nicks" coloridos, muito longos, ofensivos ou com caracteres "não comuns".
- Não faça flood, cole no máximo três linhas de cada vez.
- Não faça flames, não incentive e não alimente os "trolls".

