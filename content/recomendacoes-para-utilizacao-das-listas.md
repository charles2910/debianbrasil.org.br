---
title: "Recomendacoes para utilização das listas"
kind: page
created_at: 2020-09-13 20:09
author: Paulo Henrique de Lima Santana
---

# Recomendações para utilização das Listas

Esse documento é fortemente baseado na
[RFC1855](http://www.apps.ietf.org/rfc/rfc1855.html) e nas
[instruções e recomendações](http://www.debian.org/MailingLists/) das
[listas de discussão](http://lists.debian.org/) do
[Projeto Debian](http://www.debian.org/).

Também é fortemente baseado nas
[Regras de Conduta para as Listas do Debian-BR](http://www.debianbrasil.org/docs/nacionais/sgml/lista/lista.html/index.html)
criado pelo Debian Brasil.

## 1 - Introdução

O Projeto Debian-BR tem participantes distribuídos por todo o Brasil. Portanto,
o e-mail é o meio preferido para discutir vários assuntos pertinentes à
Comunidade e ao Projeto.

Nossa lista é aberta para todo mundo, o que significa que qualquer um pode ler
tudo o que é publicado, e participar das discussões. Qualquer um pode participar
de nossa comunidade e espalhar o software livre pelo mundo.

Há um software de processamento automático de mail chamado MailMan, que fica
num servidor chamado
[alioth](http://lists.alioth.debian.org/cgi-bin/mailman/listinfo) onde estão a
maioria dos Grupos de Usuários Debian Brasileiros. Todas as mensagens de
pedidos de inscrição, cancelamento de inscrição e submissão têm que ser
mandadas para endereços específicos nessa máquina, no caso específico de nossa
lista, você pode obter mais informações nesse
[link](https://lists.alioth.debian.org/mailman/listinfo/debian-br-geral).

## 2 - Uso básico

A utilização é bastante simples, para enviar mensagens inscreva-se na
[página de detalhes da lista debian-br-geral](https://lists.alioth.debian.org/mailman/listinfo/debian-br-geral).

## 3 - Aviso / Política de privacidade / Informação legal

As listas de discussão são fóruns públicos. Todas as mensagens enviadas para as
listas são distribuídas para os assinantes da lista e copiadas para o arquivo
público, para que as pessoas naveguem ou procurem sem a necessidade de serem
assinantes da lista.

Podem haver outros lugares onde nossas listas são distribuídas -- certifique-se
de nunca enviar qualquer material confidencial ou não licenciado para elas. Isto
inclui itens como endereços de e-mail. Uma nota particular é o fato que
spammers, vírus, worms, etc. são conhecidos por abusar dos endereços de e-mail
postados em nossas listas.

O Debian-BR mantêm as listas de discussão com boa fé e irão seguir os passos
necessários para minimizar todo o abuso notado e manter o serviço normal e
ininterrupto. Ao mesmo tempo, não são responsáveis por todas as mensagens nas
listas de discussão e nem por algo que possa acontecer em relação a elas.

## 4 - Aviso de responsabilidade para a lista de discussão do debian-br-geral

Nossas listas de discussão são fóruns públicos, e os arquivos de nossas listas
de discussão também são públicos.

Enviando uma mensagem para um fórum público, você concorda com a distribuição
pública de seu artigo. Todas as mensagens enviadas nossa lista de discussão
serão distribuídas publicamente e arquivadas nos nossos arquivos de listas de
discussão.

Todas as mensagens enviadas por uma pessoa diretamente para a lista, ou
respostas de outros para essas mensagens, são consideredas públicas.

Obviamente o autor ainda possuí o copyright do conteúdo destas mensagens que ele
escreveu. No entanto, isto não significa que o Debian e/ou Debian-BR têm a
obrigação de removê-las dos arquivos das listas após sua publicação.

Os arquivos das listas de discussão têm sido públicos desde antes de você enviar
uma mensagem para o endereço da lista de discussão. Você é responsável por
determinar para quem você está enviando sua mensagem. Você não pode enviar
mensagens para recipientes arbitrários e esperar que eles sejam automaticamente
forçados a aceitar seus termos para receber suas mensagens.

A comunidade Debian-BR não aceita nenhuma responsabilidade pelas opiniões e
informações enviadas para sua lista de discussão por outros. Nossa comunidade
se isenta de todas as garantias com relação a informações enviadas para suas
listas de discussão, enviadas por outros, esta isenção inclui todas as garantias
implicitas de mercantilidade e funcionalidade para um dado propósito. Em nenhum
evento o Debian-BR será responsável por quaisquer danos especiais, indiretos ou conseqüênciais, ou danos de qualquer tipo, resultando em perca de usabilidade,
dados ou lucros, vindos de ou em conexão com o uso de qualquer informação
enviada para a lista de discussão do Debian-BR.

Enviando materiais, as partes enviantes garantem e representam que elas possuem
o copyright que diz respeito a tais materiais, que receberam permissão do dono
do copyright ou que o material está em domínio público. Esta parte enviante
também garante e representa que ela possue o direito completo a enviar tal
material e que tal envio não irá infringir quaisquer direitos ou interesses de
outros.

A Debian e o Debian-BR geralmente não monitoram suas listas de discussão por
envios impróprios, e não fazem controle editorial das mensagens. No entanto, nós
nos reservamos o direito de previnir o envio para as listas de discussão no
evento de falha para cumprir o código de conduta da lista de discussão.

## 5 - Código de Conduta

Quando utilizando a lista do Debian-BR, por favor siga as seguintes regras:

- Não envie SPAM! Se você está em dúvida se sua mensagem será ou não considerada
como SPAM, consulte os [moderadores](debian-br-geral-owner@lists.alioth.debian.org).
- Envie suas mensagens em Português Brasileiro. Só utilize outras línguas
quando isso for necessário ou explicitamente permitido.
- Prenda-se ao assunto, o escopo dessa lista são os projetos realizados pelo
Debian-BR, utilize a <debian-user-portuguese@lists.debian.org> para suporte.
- Limite seu texto a 72 caracteres. Linhas com mais de 80 caracteres são
aceitáveis para mensagens geradas por computador (ex: ls -l).
- Não envie mensagens automáticas avisando que você está de férias ou "fora
do escritório".
- Não envie pedidos de inscrição ou desinscrição para o endereço da lista,
utilize o  [site apropriado](https://lists.alioth.debian.org/mailman/listinfo/debian-br-geral).
- Não envie mensagens em HTML, XHTML, RTF (...), utilize texto plano e puro.
- Não envie anexos com mais de 40KB (eles ficarão pendentes na fila de moderação).
- Quando responder mensagens para a lista, não envie cópias (CC) para o
remetente, a menos que ele tenha explicitamente requisitado.
- Evite linguagem de baixo calão e "palavrões", algumas pessoas utilizam lugares
públicos ou empresas para ler suas mensagens.
- Tente não criar "flames", não é educado.
- Leia a [RFC1855](http://www.apps.ietf.org/rfc/rfc1855.html), há uma versão
[traduzida](http://www.abusar.org.br/rfc.html).
- Em caso de dúvida ou para maiores esclarecimentos, consulte os
[moderadores](debian-br-geral-owner@lists.alioth.debian.org).
- As pessoas pagam pela conexão delas, utilize assinaturas com um máximo de
oito linhas.
- Mensagens que não dizem respeito ao escopo da lista devem ser marcadas como
"off-topic", para isso utilize uma das seguintes opções: [OT] ou [OFF-TOPIC],
isso facilita a criação de filtros nos clientes de e-mail.

Qualquer violação dessas regras leva à moderação, podendo levar à suspensão e
finalmente à expulsão/banimento da lista.

## 6 - Sobre os Moderadores

Os atuais moderadores foram escolhidos pelo criador da lista. A escolha foi
baseada na confiança e credibilidade.

Com a estruturação do projeto a moderação passa a ser decidida internamente já
que a lista pertence à comunidade. Sendo assim os moderadores são escolhidos
pelo Líder da Comunidade GUD-BR-PR e pelos atuais moderadores.

Qualquer pessoa está apta a atuar como um moderador, no entanto, algumas
considerações são válidas:

- Qualquer pessoa pode pedir moderação na lista, a diferença é que os
moderadores tem a habilidade de atuar de maneira corretiva, realizando bloqueio
e suspensão de contas.
- Não peça para tornar-se moderador, provavelmente você será convidado para
participar de acordo com seu perfil e comportamento dentro da lista.
- Os moderadores tentam atuar de forma transparente e imparcial, não é possível
agradar a todos, por isso, os moderadores utilizam esse documento para tomar
suas decisões.
